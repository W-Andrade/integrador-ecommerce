angular.module('CIntegrator', ['ui.bootstrap', 'ngResource', 'ui.router', 'ngCookies', 'ui.mask', '720kb.datepicker', 'ngStorage', 'blockUI','scrollable-table', 'oitozero.ngSweetAlert']);

angular.module("CIntegrator").filter('filterSoma', [function () {
        return function (arr, idx) {
            return arr.reduce(function (previousValue, currentValue) {
                return parseFloat(previousValue) + parseFloat(currentValue[idx]);
            }, 0);
        };
    }]);


