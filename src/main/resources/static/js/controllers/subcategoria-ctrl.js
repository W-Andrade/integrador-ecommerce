/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('SubCategoriaCtrl', ['$scope', 'CategoriaService', SubCategoriaCtrl]);

function SubCategoriaCtrl($scope, CategoriaService) {
    var self = this;
    self.paramConsulta = {};
    self.itemSelecionado = new CategoriaService();
    self.departamentos = [];
    self.categorias = [];
    self.itens = [];



    self.initSubCategorias = function () {
        CategoriaService.departamentos(function (response) {
            self.departamentos = response;
        });

        CategoriaService.subcategorias(function (response) {
            self.itens = response;
        });
    };

    self.selectCategoriaByDept = function () {
        CategoriaService.categoriaBy(self.departamento, function (response) {
            self.categorias = response;
        });
    };



    self.save = function () {
        self.itemSelecionado.$save(function (response) {
            self.initSubCategorias();
            self.itemSelecionado = new CategoriaService();
        });
    };

    self.searchCategoria = function () {
        self.paramConsulta.tipo = "categoria";
        CategoriaService.search(self.paramConsulta, function (response) {
            self.itens = response;
        }, function (err) {
            console.info(err);
        });
    };

    self.delete = function (item) {
        CategoriaService.delete({id: item.id}, function () {
            self.initSubCategorias();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relamente excluir item ?")) {
            self.delete(item);
        }

    };

    self.select = function (it) {
        self.itemSelecionado = it;
        self.departamento = it.categoriaPai.categoriaPai;
        self.selectCategoriaByDept();
        $scope.activeTabSubCategoria = 1;
    };
    
    self.clear = function (){
        self.itemSelecionado = new CategoriaService();
        $scope.activeTabSubCategoria = 0;
    }


}