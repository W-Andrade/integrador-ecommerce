/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('EstoqueCtrl', ['$scope', 'EstoqueService', 'ProdutoService', EstoqueCtrl]);

function EstoqueCtrl($scope, EstoqueService, ProdutoService) {
    var self = this;
    self.itens = [];
    self.itemSelecionado = new EstoqueService();
    self.currentPage = 1;
    self.paramConsulta = {};
    self.paramDefault = {
        ufs: ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PR',
            'PB', 'PA', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SE', 'SP', 'TO']
    };

    self.init = function () {

    };

    self.searchProduto = function () {
        if (self.paramConsulta.code !== undefined
            || (self.paramConsulta.desc !== undefined && self.paramConsulta.desc.length > 2)
            || self.paramConsulta.isEstoqueZero
            || self.paramConsulta.isPrecoZero) {


            EstoqueService.search(self.paramConsulta, function (response) {
                self.itens = response;
            }, function (err) {
                console.info(err);
            });
        }
    };

    self.save = function () {
        if (self.itemSelecionado.id !== undefined) {
            self.itemSelecionado.statusIntegracaoPreco = "NAO_INTEGRADO";
            self.itemSelecionado.statusIntegracaoEstoque = "NAO_INTEGRADO";

            self.itemSelecionado.$save(function (response) {
                self.itemSelecionado = new EstoqueService();
            });
        } else {
            window.alert("Selecione um produto para executar esta operação");
        }
    };

    self.delete = function (item) {
        EstoqueService.delete({id: item.id}, function () {
            self.init();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja ralmente excluir o item ?")) {
            self.delete(item);
        }
    };

    self.select = function (it) {
        self.itemSelecionado = it;
        $scope.activeTabEstoque = 1;
    };


    self.clear = function () {
        self.itemSelecionado = new EstoqueService();
        $scope.activeTabEstoque = 0;
    };

}
