/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global moment */

angular.module('CIntegrator').controller('PedidoCtrl', ['$scope', '$uibModal', '$stateParams', 'PedidoService', PedidoCtrl]);

function PedidoCtrl($scope, $uibModal, $stateParams, PedidoService) {
    var self = this;
    self.itemSelecionado = new PedidoService();
    self.itens = [];
    self.currentPage = 1;
    self.item = {};
    self.paramConsulta = {};


    self.openDetalhe = function (item) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/pedido-detalhe.html',
            templateUrl: 'myModalContent.html',
            controller: function ($uibModalInstance, item) {
                var $ctrl = this;

                $ctrl.item = item;

                $ctrl.ok = function () {
                    $uibModalInstance.close();
                };

                $ctrl.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            controllerAs: '$ctrl',
            size: 'lg',
            resolve: {
                item: function () {
                    return item;
                }
            }
        });
    };

    self.openPainelConsulta = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'teste.html',
            controller: function ($uibModalInstance, PedidoService) {
                var $ctrl = this;
                $ctrl.items = [];
                $ctrl.paramConsulta = {};

                $ctrl.pesquisar = function () {
                    if ($ctrl.paramConsulta.code !== undefined || ($ctrl.paramConsulta.data !== null)) {

                        var p = angular.copy($ctrl.paramConsulta);

                        if ($ctrl.paramConsulta.code > 0) {
                            p.data = undefined;
                        } else {
                            p.data = $ctrl.paramConsulta.data.split("/").reverse().join("-");
                        }

                        PedidoService.search(p, function (response) {
                            response.forEach(function (it) {
                                $ctrl.items.push({selected: false, itm: it})
                            });
                        }, function (err) {
                            console.info(err);
                        });
                    }

                };

                $ctrl.checkAll = function () {
                    if ($ctrl.selectedAll) {
                        $ctrl.selectedAll = true;
                    } else {
                        $ctrl.selectedAll = false;
                    }
                    angular.forEach($ctrl.items, function (item) {
                        item.selected = $ctrl.selectedAll;
                    });

                };

                $ctrl.ok = function () {

                    self.itens = [];
                    $ctrl.items.forEach(function (it) {
                        if (it.selected) {
                            self.itens.push(it.itm);
                        }
                    });


                    $uibModalInstance.close();
                };

                $ctrl.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            controllerAs: '$ctrl',
            size: 'lg'
        });

    };

    self.reprocessar = function (itm) {

        var statusAnt = itm.statusErpIntegrado;

        itm.statusErpIntegrado = "AGUARDANDO";

        PedidoService.save(itm, function () {
            //TODO add toast messagems
        }, function () {
            itm.statusErpIntegrado = statusAnt;
        });
    };

    self.reIntegrarPedidoEcommerce = function (itm) {

        PedidoService.reIntegrarPedidoEcommerce({id: itm.id}, function () {
            var index = self.itens.indexOf(itm);
            self.itens.splice(index, 1);
        });
    };


    self.removerPedidoEcommerceMonitoramento = function (itm) {

        var statusAnt = itm.statusErpIntegrado;

        itm.statusErpIntegrado = "REMOVIDO_MONITORAMENTO";

        PedidoService.save(itm, function () {
            var index = self.itens.indexOf(itm);
            self.itens.splice(index, 1);
        }, function () {
            itm.statusErpIntegrado = statusAnt;
        });

    };



    self.carregarPorStatus = function () {
        if ($stateParams.status !== undefined) {
            PedidoService.query({statusErpIntegrado: $stateParams.status}, function (response) {
                self.itens = response;
            });
        }
    };


    self.searchPedido = function () {

        if (self.paramConsulta.code !== undefined || (self.paramConsulta.data !== null)) {

            var p = angular.copy(self.paramConsulta);

            if (self.paramConsulta.code > 0) {
                p.data = undefined;
            } else {

                p.data = self.paramConsulta.data.split("/").reverse().join("-");
            }



            PedidoService.search(p, function (response) {
                self.itens = response;
            }, function (err) {
                console.info(err);
            });
        }

    };

    self.detalhePedido = function () {

        if ($stateParams.id !== undefined) {

            PedidoService.get({id: $stateParams.id}, function (response) {
                self.item = response;
            });
        }
    };

    self.init = function () {
        PedidoService.query(function (response) {
            self.itens = response;
        });
    };

    self.save = function () {
        self.itemSelecionado.$save(function (response) {
            self.itens.push(response);
            self.itemSelecionado = new PedidoService();
        });
    };

    self.delete = function (item) {
        PedidoService.delete({id: item.id}, function () {
            self.init();
        });
    };

    self.askDelete = function (item) {
        if (window.confirm("Deseja relmente excluir item ?")) {
            self.delete(item);
        }
    };

    self.select = function (it) {
        self.itemSelecionado = it;
    };


}
