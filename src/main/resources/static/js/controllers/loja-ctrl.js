/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('LojaCtrl', ['$scope', 'LojaService', LojaCtrl]);

function LojaCtrl($scope, LojaService){
    var self = this;
    self.itemSelecionado = new LojaService();
    self.itens = [];
    self.paramDefault = {
        ufs: ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PR',
            'PB','PA','PE','PI','RJ','RN','RS','RO','RR','SC','SE','SP','TO']
    }
    
    self.init = function(){
        LojaService.query(function (response){
            self.itens = response;
        });
    };
    
    self.save = function(){
        self.itemSelecionado.$save(function (response){
            self.itens.push(response);
            self.itemSelecionado = new LojaService();
        });
    };
    
    self.delete = function(id){
        LojaService.delete({id: id}, function () {
           self.init(); 
        });
    };
    
    self.askDelete = function(item){
        if(window.confirm("Deseja relmente excluir item ?")){
           console.log("item: "+ item.id.valueOf());
           self.delete(item.id.valueOf());
        }
    };
    
    self.select = function (it){
        self.itemSelecionado = it;
        $scope.activeTabLoja = 1;
    };
    
    self.clear = function () {
        self.itemSelecionado = new LojaService();
        $scope.activeTabLoja = 0;
    }
    
    self.init();
}
