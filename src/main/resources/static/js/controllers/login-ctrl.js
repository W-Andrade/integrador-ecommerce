angular.module('CIntegrator').controller('LoginCtrl', ['$rootScope', '$state', '$http', 'UserService', LoginCtrl]);


function LoginCtrl($rootScope, $state, $http, UserService) {

    var login = this;
    function signIn(user) {

        UserService.setCurrentUser(user);

        $http.get('api/usuario/by/' + user.username).then(function (response) {
            UserService.setCurrentUser(response.data);
            $rootScope.$broadcast('authorized');
            $state.go('index');
        });
    }

    function submit(user) {
        signIn(user);
    }
    login.submit = submit;
    login.signIn = signIn;
}