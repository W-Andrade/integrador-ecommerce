/**
 * Master Controller
 */

angular.module('CIntegrator')
        .controller('MasterCtrl', ['$rootScope', '$state', '$scope', '$cookieStore', '$http', 'UserService', MasterCtrl]);

function MasterCtrl($rootScope, $state, $scope, $cookieStore, $http, UserService) {

    var main = this;
    main.applicationVersion = '0.0.0';
    function logout() {
        main.currentUser = UserService.setCurrentUser(null);
        $state.go('login');
    }
    $rootScope.$on('authorized', function () {
        main.currentUser = UserService.getCurrentUser();
    });
    $rootScope.$on('unauthorized', function () {
        main.currentUser = UserService.setCurrentUser(null);
        $state.go('login');
    });
    main.logout = logout;
    main.currentUser = UserService.getCurrentUser();


    main.init = function () {
        main.getVersion();

        $http.get('application/profiles').then(function (restult) {
            $rootScope.application = restult.data.profiles;
        });

    };


    main.getVersion = function () {
        $http.get('application/version').then(function (restult) {
            main.applicationVersion = restult.data.version;
        });
    };

    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;

    $scope.getWidth = function () {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function (newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = !$cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function () {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function () {
        $scope.$apply();
    };

}