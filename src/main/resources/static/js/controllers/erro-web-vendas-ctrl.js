/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('CIntegrator').controller('ErroWebVendaCtrl', ['$scope', 'ErroWebVendaService', ErroWebVendaCtrl]);

function  ErroWebVendaCtrl($scope, ErroWebVendaService) {
    var self = this;
    self.itens = [];
    self.currentPage = 1;


    self.init = function () {
        ErroWebVendaService.query(function (response) {
            self.itens = response;
        });
    };

    self.deleteAll = function () {
        ErroWebVendaService.deleteAll();
        self.init();
    };

    self.askDelete = function () {
        if (window.confirm("Deseja ralmente excluir o item ?")) {
            self.deleteAll();
        }
    };


    self.download = function (content, filename) {


        if (!filename) {
            filename = 'tmp.txt';
        }


        var pom = document.createElement('a');
        pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
        pom.setAttribute('download', filename);
        pom.setAttribute('target', '_blank');

        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            pom.dispatchEvent(event);
        } else {
            pom.click();
        }

    };

}
