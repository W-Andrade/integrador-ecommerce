angular.module('CIntegrator').service('SecurityInterceptor', ['$rootScope', 'UserService', SecurityInterceptor]);

function SecurityInterceptor($rootScope, UserService) {
    var service = this;
    service.request = function (config) {
        var currentUser = UserService.getCurrentUser();

        if (currentUser) {
            var oauth = OAuth({
                consumer: {
                    'public': currentUser.username,
                    'secret': currentUser.password
                },
                signature_method: 'HMAC-SHA1'
            });


            var query = [];
            if (config.method === 'GET' || config.method === 'DELETE') {
                Object.keys(config.params || {}).forEach(function (key) {
                    var val = config.params[key];
                    if (!angular.isUndefined(val)) {
                        query.push([key, val].join('=')); // maybe url encode
                    }
                });
            }else{
                config.params = undefined;
            }

            var queryStr = query.length > 0 ? '?' + query.join('&') : '';


            var request_data = {
                url: location.origin + location.pathname + config.url + queryStr,
                method: config.method
            };
            config.headers["Authorization"] = oauth.toHeader(oauth.authorize(request_data))['Authorization'];
        }

        return config;

    };
    service.responseError = function (response) {
        if (response.status === 401 || response.status === 403) {
            $rootScope.$broadcast('unauthorized');
        }else if(response.status === 400){
            alert(response.data.message);
        }
        return response;
    };

}