/**
 * 
 */
angular.module('CIntegrator').factory('CategoriaService', ['$resource', CategoriaService]);

function CategoriaService($resource) {
    return $resource('api/categoria/:id', {
        query: '@id'
    }, {
        search: {
            method: 'GET',
            cache: false,
            url: 'api/categoria/search',
            isArray: true
        },
        departamentos: {
            method: 'GET',
            cache: false,
            url: 'api/categoria/departamentos',
            isArray: true
        },
        categorias: {
            method: 'GET',
            cache: false,
            url: 'api/categoria/categorias',
            isArray: true
        },
        subcategorias: {
            method: 'GET',
            cache: false,
            url: 'api/categoria/subcategorias',
            isArray: true
        },
        categoriaBy: {
            method: 'POST',
            cache: false,
            url: 'api/categoria/categoriaBy',
            isArray: true
        }
    });
}