/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('CIntegrator').factory('ErroWebVendaService', ['$resource', ErroWebVendaService]);

function ErroWebVendaService($resource) {
    return $resource('api/webvenda/erro/:id', {
        query: '@id'
    }, {
        deleteAll: {
            method: 'DELETE',
            cache: false,
            url: 'api/webvenda/erro/clean-all'
        }
    });
}