/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('PedidoProdutoService', ['$resource', PedidoProdutoService]);

function PedidoProdutoService($resource){
    return $resource('api/pedidoproduto/:id', {
        query: '@id'
    });
}

