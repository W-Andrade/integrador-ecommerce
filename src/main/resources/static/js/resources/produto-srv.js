/**
 * 
 */
angular.module('CIntegrator').factory('ProdutoService', ['$resource', ProdutoService]);

function ProdutoService($resource){
    return $resource('api/produto/:id', {
        query: '@id'
    }, {
        search: {
            method: 'GET',
            cache: false,
            url : 'api/produto/search',
            isArray: true
        }
    });
}