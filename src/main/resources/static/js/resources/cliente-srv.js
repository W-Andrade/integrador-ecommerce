/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('ClienteService',['$resource',ClienteService]);

function ClienteService($resource){
    return $resource('api/cliente/:id',{
        query: '@id'
    }, {
        search: {
            method: 'GET',
            cache: false,
            url : 'api/cliente/search',
            isArray: true
        }
    });
}

    