/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('CIntegrator').factory('JobService',['$resource',JobService]);

function JobService($resource){
    return $resource('api/job/:id',{
        query: '@id'
    }, {
        processarManual: {
            method: 'GET',
            cache: false,
            url : 'api/job/manual'
        }
    });
}