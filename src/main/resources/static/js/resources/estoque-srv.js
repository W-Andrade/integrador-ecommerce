/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('CIntegrator').factory('EstoqueService',['$resource',EstoqueService]);

function EstoqueService($resource){
    return $resource('api/estoque/:id',{
        query: '@id'
    }, {
        getEstoqueProduto: {
            method: 'POST',
            cache: false,
            url : 'api/estoque/produto'
        },
        search: {
            method: 'GET',
            cache: false,
            url : 'api/estoque/search',
            isArray: true
        }
    });
}


