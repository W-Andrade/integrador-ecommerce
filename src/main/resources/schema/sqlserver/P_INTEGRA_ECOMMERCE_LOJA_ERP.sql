IF  OBJECT_ID('P_INTEGRA_ECOMMERCE_LOJA_ERP') IS NOT NULL
BEGIN
	DROP PROCEDURE P_INTEGRA_ECOMMERCE_LOJA_ERP
END

;

CREATE PROCEDURE P_INTEGRA_ECOMMERCE_LOJA_ERP
AS
BEGIN

INSERT INTO INTEGRA_ECOMMERCE_LOJA
(
data_altercao
,data_cadastro
,bairro
,cep
,cnpj
,codigo_eco
,codigo_erp
,codigo_municipio
,complemento
,inscricao_estadual
,inscricao_municipal
,logradouro
,municipio
,nome
,numero
,razao_social
,status_integracao
,telefone
,uf
)

select distinct
getdate() as data_altercao
,getdate () as data_cadastro
,bairro as bairro
,dbo.f_limpa_numeros(prc_filial.cep) as cep
,dbo.f_limpa_numeros(cgc) as cnpj
,cd_filial as codigo_eco
,cd_filial as codigo_erp
,glb_municipio.COD_MUNICIPIO_SINTEGRA as codigo_municipio
,complemento as complemento
,insc_est as inscricao_estadual
,insc_muni as inscricao_municipal
,END_FILILAL as logradouro
,glb_municipio.ds_municipio as municipio
,nm_fant as nome
,null as numero
,rz_filial as razao_social
,0 as status_integracao
,dbo.f_limpa_numeros(tel) as telefone
,glb_cid.uf as uf

from

prc_filial
inner join glb_cid
on
prc_filial.cd_cid = glb_cid.cd_cid
inner join glb_cid_glb_municipio
on
glb_cid_glb_municipio.cd_cid = glb_cid.cd_cid
inner join glb_municipio
on
glb_cid_glb_municipio.cd_municipio = glb_municipio.cd_municipio

where

CD_EMP = 1
AND sts_filial = 0
AND dbo.f_limpa_numeros(cgc) != '00000000000000'

AND NOT EXISTS
(
	SELECT ID FROM INTEGRA_ECOMMERCE_LOJA WHERE CODIGO_ERP = CD_FILIAL
)



END

