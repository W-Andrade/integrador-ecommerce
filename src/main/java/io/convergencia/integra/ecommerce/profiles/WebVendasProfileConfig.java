/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.profiles;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author wanderson
 */
@Profile("web-vendas")
@Configuration(value = "configWebVendasProfileConfig")
@EnableScheduling
@ComponentScan(basePackages = "io.convergencia.integra.webvendas")
@EnableJpaRepositories(value = "io.convergencia.integra.webvendas.repository")
@EntityScan("io.convergencia.integra.webvendas.model")
public class WebVendasProfileConfig {
}
