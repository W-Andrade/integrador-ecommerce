/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.profiles;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author wanderson
 */
@Profile("erp")
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "io.convergencia.integra.erp")
@EnableJpaRepositories(value = "io.convergencia.integra.erp.repository")
@EntityScan("io.convergencia.integra.erp.model")
public class ERPProfileConfig {

    
    //io.convergencia.integra.erp.model
}
