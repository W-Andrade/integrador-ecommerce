/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.profiles;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 *
 * @author wanderson
 */

@Profile("qual-farmacia")
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "io.convergencia.integra.qualfarmacia")
@EnableJpaRepositories(value = "io.convergencia.integra.qualfarmacia.repository")
@EntityScan("io.convergencia.integra.qualfarmacia.model")
public class QualFarmaciaProfileConfig implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutorQualFarmacia());
    }

    @Bean(destroyMethod = "shutdown")
    public Executor taskExecutorQualFarmacia() {
        return Executors.newScheduledThreadPool(5);
    }

}