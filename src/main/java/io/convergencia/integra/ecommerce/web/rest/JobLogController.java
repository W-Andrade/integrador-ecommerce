/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.model.JobLog;
import io.convergencia.integra.ecommerce.repository.JobLogDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wanderson
 */
@RestController
@RequestMapping(value = "/api/job-log")
public class JobLogController {
    @Autowired
    private JobLogDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<JobLog> getAll() {
        List<JobLog> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }
    
    @RequestMapping(value = "/{nome:[\\D]+}", method = RequestMethod.GET)
    public List<JobLog> getByName(@PathVariable String nome) {
        List<JobLog> target = new ArrayList<>();
        dao.findTop150ByJobNameOrderByIdDesc(nome).forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id:[\\d]+}", method = RequestMethod.GET)
    public JobLog getById(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }
}
