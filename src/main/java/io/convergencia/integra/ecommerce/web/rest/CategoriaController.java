/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.model.Categoria;
import io.convergencia.integra.ecommerce.repository.CategoriaDao;
import io.convergencia.integra.ecommerce.specifications.CategoriaSpecification;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wanderson
 */
@RestController
@RequestMapping(value = "/api/categoria")
public class CategoriaController {

    @Autowired
    private CategoriaDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<Categoria> person() {

        List<Categoria> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Categoria person(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(value = "/departamentos", method = RequestMethod.GET)
    public List<Categoria> departamentos() {
        List<Categoria> target = new ArrayList<>();
        dao.departamentos().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/categorias", method = RequestMethod.GET)
    public List<Categoria> categorias() {
        List<Categoria> target = new ArrayList<>();
        dao.categorias().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/subcategorias", method = RequestMethod.GET)
    public List<Categoria> subcategorias() {
        List<Categoria> target = new ArrayList<>();
        dao.subcategorias().forEach(target::add);
        return target;
    }
    
    @RequestMapping(value = "/categoriaBy",method = RequestMethod.POST)
    public List<Categoria> categoriaBy(@RequestBody Categoria person) {
        List<Categoria> target = new ArrayList<>();
        dao.findByCategoriaPai(person).forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<Categoria> prodBy(@QueryParam("code") Integer code, @QueryParam("desc") String desc, @QueryParam("tipo") String tipo) {
        Specification<Categoria> searchPagamento = CategoriaSpecification.search(code, desc, tipo);
        List<Categoria> target = new ArrayList<>();
        dao.findAll(searchPagamento).forEach(target::add);
        return target;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody Categoria person) {
        dao.save(person);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

}
