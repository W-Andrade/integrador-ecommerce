/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.model.Cliente;
import io.convergencia.integra.ecommerce.repository.ClienteDao;
import java.util.ArrayList;
import java.util.List;

import io.convergencia.integra.ecommerce.specifications.ClienteIndexSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

/**
 *
 * @author Wanderson
 */
@RestController
@RequestMapping(value = "/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteDao dao;

    @RequestMapping(method = RequestMethod.GET)
    public List<Cliente> person() {
        List<Cliente> target = new ArrayList<>();
        dao.findAll().forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<Cliente> prodBy(@QueryParam("code") Integer code, @QueryParam("desc") String desc) {
        Specification<Cliente> searchPagamento = ClienteIndexSpecification.search(code, desc);
        List<Cliente> target = new ArrayList<>();
        dao.findAll(searchPagamento).forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Cliente person(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody Cliente person) {
        dao.save(person);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

}
