/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.enuns.EStatusErpIntegrado;
import io.convergencia.integra.ecommerce.enuns.EStatusIntegracao;
import io.convergencia.integra.ecommerce.repository.EstoqueDao;
import io.convergencia.integra.ecommerce.repository.LojaDao;
import io.convergencia.integra.ecommerce.repository.PedidoDao;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wanderson
 */
@RestController
@RequestMapping(value = "/api/dashboard")
public class DashboardController {

    @Autowired
    private PedidoDao pedidoDao;
    
    @Autowired
    private EstoqueDao estoqueDao;
    
    @Autowired
    private LojaDao lojaDao;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public Map<String, Map<String, Long>> v() {

        Map<String, Map<String, Long>> m = new HashMap<>();

        m.put("pedido", new HashMap<>());
        m.put("estoque", new HashMap<>());
        m.put("preco", new HashMap<>());
        m.put("loja", new HashMap<>());

        m.get("pedido").put(EStatusErpIntegrado.AGUARDANDO.name(), pedidoDao.countByStatusErpIntegrado(EStatusErpIntegrado.AGUARDANDO));
        m.get("pedido").put(EStatusErpIntegrado.ERRO.name(), pedidoDao.countByStatusErpIntegrado(EStatusErpIntegrado.ERRO));
        m.get("pedido").put(EStatusErpIntegrado.INTEGRADO.name(), pedidoDao.countByStatusErpIntegrado(EStatusErpIntegrado.INTEGRADO));
        m.get("pedido").put(EStatusErpIntegrado.REMOVIDO_MONITORAMENTO.name(), pedidoDao.countByStatusErpIntegrado(EStatusErpIntegrado.REMOVIDO_MONITORAMENTO));
        
        m.get("estoque").put(EStatusIntegracao.ERRO.name(), estoqueDao.countByStatusIntegracaoEstoque(EStatusIntegracao.ERRO));
        m.get("estoque").put(EStatusIntegracao.INTEGRADO.name(), estoqueDao.countByStatusIntegracaoEstoque(EStatusIntegracao.INTEGRADO));
        m.get("estoque").put(EStatusIntegracao.NAO_INTEGRADO.name(), estoqueDao.countByStatusIntegracaoEstoque(EStatusIntegracao.NAO_INTEGRADO));
        
        m.get("preco").put(EStatusIntegracao.ERRO.name(), estoqueDao.countByStatusIntegracaoPreco(EStatusIntegracao.ERRO));
        m.get("preco").put(EStatusIntegracao.INTEGRADO.name(), estoqueDao.countByStatusIntegracaoPreco(EStatusIntegracao.INTEGRADO));
        m.get("preco").put(EStatusIntegracao.NAO_INTEGRADO.name(), estoqueDao.countByStatusIntegracaoPreco(EStatusIntegracao.NAO_INTEGRADO));

        m.get("loja").put(EStatusIntegracao.ERRO.name(), lojaDao.countByStatusIntegracao(EStatusIntegracao.ERRO));
        m.get("loja").put(EStatusIntegracao.INTEGRADO.name(), lojaDao.countByStatusIntegracao(EStatusIntegracao.INTEGRADO));
        m.get("loja").put(EStatusIntegracao.NAO_INTEGRADO.name(), lojaDao.countByStatusIntegracao(EStatusIntegracao.NAO_INTEGRADO));

        return m;
    }
}
