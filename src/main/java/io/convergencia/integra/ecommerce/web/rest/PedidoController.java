/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.web.rest;

import io.convergencia.integra.ecommerce.enuns.EStatusErpIntegrado;
import io.convergencia.integra.ecommerce.model.Pedido;
import io.convergencia.integra.ecommerce.model.PedidoIndex;
import io.convergencia.integra.ecommerce.repository.PedidoDao;
import io.convergencia.integra.ecommerce.repository.PedidoIndexDao;
import io.convergencia.integra.ecommerce.specifications.PedidoIndexSpecification;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wanderson
 */
@RestController
@RequestMapping(value = "/api/pedido")
public class PedidoController {

    @Autowired
    private Environment env;

    @Autowired
    private PedidoDao dao;

    @Autowired
    private PedidoIndexDao pedidoIndexDao;

    @RequestMapping(method = RequestMethod.GET)
    public List<PedidoIndex> person(@RequestParam("statusErpIntegrado") EStatusErpIntegrado esei) {
        List<PedidoIndex> target = new ArrayList<>();
        List<EStatusErpIntegrado> c = new ArrayList<>();

        if (esei == null) {
            c.add(EStatusErpIntegrado.AGUARDANDO);
            c.add(EStatusErpIntegrado.ERRO);
        } else {
            c.add(esei);
        }

        pedidoIndexDao.findByStatusErpIntegradoIn(c).forEach(target::add);
        return target;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Pedido person(@PathVariable Long id) {
        return dao.findOne(id);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public List<PedidoIndex> prodBy(@QueryParam("code") Integer code, @QueryParam("data") @DateTimeFormat(pattern = "yyyy-MM-dd") Date data) {
        Specification<PedidoIndex> searchPagamento = PedidoIndexSpecification.search(code, data);
        List<PedidoIndex> target = new ArrayList<>();
        pedidoIndexDao.findAll(searchPagamento).forEach(target::add);
        return target;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> save(@RequestBody Pedido person) {
        dao.save(person);
        return ResponseEntity.ok(null);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dao.delete(id);
        return ResponseEntity.ok(null);
    }

}
