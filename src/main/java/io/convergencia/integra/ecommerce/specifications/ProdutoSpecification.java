/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.specifications;

import io.convergencia.integra.ecommerce.model.Produto;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author wanderson
 */
public class ProdutoSpecification {

    public static Specification<Produto> search(final Integer cdProd, final String descricao) {
        return (Root<Produto> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();
            
            if (cdProd != null && cdProd > 0) {
                predicates.add(cb.equal(root.<Integer>get("codigo"), cdProd));
            } else {
                if (descricao != null && descricao.length() > 0) {
                    predicates.add(cb.like(root.<String>get("descricao"), descricao + "%"));
                }
            }
            
            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
