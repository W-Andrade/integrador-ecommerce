/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.specifications;

import io.convergencia.integra.ecommerce.model.Cliente;
import io.convergencia.integra.ecommerce.model.PedidoIndex;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author wanderson
 */
public class ClienteIndexSpecification {

    public static Specification<Cliente> search(final Integer id, final String nome) {
        return (Root<Cliente> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (id != null && id > 0) {
                predicates.add(cb.equal(root.<Integer>get("id"), id));
            } else {
                if (nome != null && nome.length() > 0) {
                    predicates.add(cb.like(root.<String>get("nome"), nome + "%"));
                }
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
