/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.specifications;

import io.convergencia.integra.ecommerce.model.Estoque;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;

import io.convergencia.integra.ecommerce.model.Produto;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author wanderson
 */
public class EstoqueSpecification {

    public static Specification<Estoque> integrar(final Boolean preco) {
        return (Root<Estoque> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (preco != null && preco) {
                predicates.add(cb.equal(root.<Boolean>get("statusIntegracaoPreco"), false));
            } else {
                predicates.add(cb.equal(root.<Boolean>get("statusIntegracaoEstoque"), false));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

    public static Specification<Estoque> searchEstoue(final Integer cdProd, final String descricao, final Boolean isPrecoZerado, final Boolean isEstoqueZerado) {

        return (Root<Estoque> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();


            if (cdProd != null && cdProd > 0) {
                predicates.add(cb.equal(root.join("produto", JoinType.INNER).get("id"), cdProd));
            } else {
                if (descricao != null && descricao.length() > 0) {
                    predicates.add(cb.like(root.join("produto", JoinType.INNER).get("descricao"), descricao + "%"));
                }
            }

            if (isEstoqueZerado != null && isEstoqueZerado) {
                predicates.add(cb.equal(root.<Integer>get("quantidade"), 0));
            }

            if (isPrecoZerado != null && isPrecoZerado) {
                predicates.add(cb.and(cb.equal(root.<Integer>get("precoVenda"), 0), cb.equal(root.<Integer>get("precoOferta"), 0)));
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };

    }
}
