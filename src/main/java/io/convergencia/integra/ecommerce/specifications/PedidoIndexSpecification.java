/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.convergencia.integra.ecommerce.specifications;

import io.convergencia.integra.ecommerce.model.PedidoIndex;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

/**
 *
 * @author wanderson
 */
public class PedidoIndexSpecification {

    public static Specification<PedidoIndex> search(final Integer cdPedEcommere, final Date data) {
        return (Root<PedidoIndex> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            final Collection<Predicate> predicates = new ArrayList<>();

            if (cdPedEcommere != null && cdPedEcommere > 0) {
                predicates.add(cb.equal(root.<Integer>get("codigoPedido"), cdPedEcommere));
            } else {
                if (data != null) {

                    Calendar ci = Calendar.getInstance();
                    ci.setTime(data);
                    ci.set(Calendar.HOUR_OF_DAY, 0);
                    ci.set(Calendar.MINUTE, 0);
                    ci.set(Calendar.SECOND, 0);
                    ci.set(Calendar.MILLISECOND, 0);
                    
                    Calendar cf = Calendar.getInstance();
                    cf.setTime(data);
                    cf.set(Calendar.HOUR_OF_DAY, 23);
                    cf.set(Calendar.MINUTE, 59);
                    cf.set(Calendar.SECOND, 59);
                    cf.set(Calendar.MILLISECOND, 999);

                    predicates.add(cb.between(root.<Date>get("dataCompra"), ci.getTime(), cf.getTime()));
                }
            }

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
