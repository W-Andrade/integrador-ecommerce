package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.enuns.EStatusErpIntegrado;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.PedidoIndex;
import java.util.List;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

@RepositoryRestResource(collectionResourceRel = "pedidos-index", path = "pedido-index")
public interface PedidoIndexDao extends PagingAndSortingRepository<PedidoIndex, Long>, JpaSpecificationExecutor<PedidoIndex> {

    @Query(value = "Select pi from PedidoIndex pi join fetch  pi.cliente c where pi.statusErpIntegrado in (?1)")
    List<PedidoIndex> findByStatusErpIntegradoIn(List<EStatusErpIntegrado> eseis);

    @Fetch(FetchMode.JOIN)
    @Override
    public List<PedidoIndex> findAll(Specification<PedidoIndex> spec);

}
