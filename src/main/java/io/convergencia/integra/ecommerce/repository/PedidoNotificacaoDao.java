package io.convergencia.integra.ecommerce.repository;

import io.convergencia.integra.ecommerce.enuns.ETipoMessage;
import io.convergencia.integra.ecommerce.model.Pedido;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import io.convergencia.integra.ecommerce.model.PedidoNotificacao;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@RepositoryRestResource(collectionResourceRel = "errosToErp", path = "erro-to-erp")
public interface PedidoNotificacaoDao extends PagingAndSortingRepository<PedidoNotificacao, Long>, JpaSpecificationExecutor<PedidoNotificacao> {


    @Query(value = "SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM PedidoNotificacao p where p.pedido = ?1 and p.tipoMessage <> 0")
    Boolean isNotificacaoErro(Pedido p);


}
